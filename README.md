This app is very basic functionality for getting the property details.

* Assumptions
    * The property data is already present in a postgres data as defined in the PDF

* Ruby version
    * 2.5.0

* Rails version
    * 5.2.1

* Configuration
    * Just the DB configuration

* Starting
    * Plain old `bundle exec rails s`

* Improvements
    * Rspecs