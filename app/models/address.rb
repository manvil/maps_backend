# frozen_string_literal: true

class Address < ApplicationRecord
  belongs_to :property
  belongs_to :lga, primary_key: 'code', foreign_key: 'lga_code'
end
