# frozen_string_literal: true

class Property < ApplicationRecord
  belongs_to :lga, primary_key: 'code', foreign_key: 'lga_code'
  has_one :address
end
