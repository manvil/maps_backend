# frozen_string_literal: true

class Lga < ApplicationRecord
  self.primary_key = 'code'

  has_many :properties
end
