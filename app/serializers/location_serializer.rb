class LocationSerializer < ActiveModel::Serializer
  attributes :id, :lga_code, :council_property_number, :longitude, :latitude, :full_address, :postcode

  def full_address
    object.address.full_address
  end

  def postcode
    object.address.postcode
  end
end
