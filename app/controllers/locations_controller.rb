# frozen_string_literal: true

class LocationsController < ApplicationController
  def index
    render json: Property.all.map { |p| [p.id, p.longitude, p.latitude] }
  end

  def show
    render json: Property.find(params[:id]), serializer: LocationSerializer
  end
end
